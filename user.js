import axios from "axios";
export class User {
    constructor(fisrtName, lastName, email, genre, age, imgUrl) {
        this.fisrtName = fisrtName;
        this.lastName = lastName;
        this.email = email;
        this.genre = genre;
        this.age = age;
        this.imgUrl = imgUrl;
    }
    get fullname() {
        return `${this.fisrtName} ${this.lastName}`;
    }
}

export function renderUsers(users) {
    let usersMarkup = users.map((user) => {
        return `
        <div class="user">
        <img src="${user.imgUrl}" alt="profil">
        <h3>${user.fullname}</h3>
        <ul>
        <li><svg width="14" height="14" viewBox="0 0 14 14" fill="none" xmlns="http://www.w3.org/2000/svg">
        <path d="M2.33335 2.33334H11.6667C12.3084 2.33334 12.8334 2.85834 12.8334 3.5V10.5C12.8334 11.1417 12.3084 11.6667 11.6667 11.6667H2.33335C1.69169 11.6667 1.16669 11.1417 1.16669 10.5V3.5C1.16669 2.85834 1.69169 2.33334 2.33335 2.33334Z" stroke="black" stroke-linecap="round" stroke-linejoin="round"/>
        <path d="M12.8334 3.5L7.00002 7.58333L1.16669 3.5" stroke="black" stroke-linecap="round" stroke-linejoin="round"/>
        </svg>
        ${user.email}</li>
        <li><svg width="14" height="14" viewBox="0 0 14 14" fill="none" xmlns="http://www.w3.org/2000/svg">
        <path d="M11.6666 7V12.8333H2.33331V7" stroke="black" stroke-linecap="round" stroke-linejoin="round"/>
        <path d="M12.8334 4.08333H1.16669V7H12.8334V4.08333Z" stroke="black" stroke-linecap="round" stroke-linejoin="round"/>
        <path d="M7 12.8333V4.08333" stroke="black" stroke-linecap="round" stroke-linejoin="round"/>
        <path d="M7.00002 4.08333H4.37502C3.98825 4.08333 3.61731 3.92969 3.34382 3.6562C3.07033 3.38271 2.91669 3.01177 2.91669 2.625C2.91669 2.23823 3.07033 1.86729 3.34382 1.5938C3.61731 1.32031 3.98825 1.16667 4.37502 1.16667C6.41669 1.16667 7.00002 4.08333 7.00002 4.08333Z" stroke="black" stroke-linecap="round" stroke-linejoin="round"/>
        <path d="M7 4.08333H9.625C10.0118 4.08333 10.3827 3.92969 10.6562 3.6562C10.9297 3.38271 11.0833 3.01177 11.0833 2.625C11.0833 2.23823 10.9297 1.86729 10.6562 1.5938C10.3827 1.32031 10.0118 1.16667 9.625 1.16667C7.58333 1.16667 7 4.08333 7 4.08333Z" stroke="black" stroke-linecap="round" stroke-linejoin="round"/>
        </svg>
        ${user.genre}</li>
        <li><svg width="14" height="14" viewBox="0 0 14 14" fill="none" xmlns="http://www.w3.org/2000/svg">
        <path d="M11.6666 7V12.8333H2.33331V7" stroke="black" stroke-linecap="round" stroke-linejoin="round"/>
        <path d="M12.8334 4.08333H1.16669V7H12.8334V4.08333Z" stroke="black" stroke-linecap="round" stroke-linejoin="round"/>
        <path d="M7 12.8333V4.08333" stroke="black" stroke-linecap="round" stroke-linejoin="round"/>
        <path d="M7.00002 4.08333H4.37502C3.98825 4.08333 3.61731 3.92969 3.34382 3.6562C3.07033 3.38271 2.91669 3.01177 2.91669 2.625C2.91669 2.23823 3.07033 1.86729 3.34382 1.5938C3.61731 1.32031 3.98825 1.16667 4.37502 1.16667C6.41669 1.16667 7.00002 4.08333 7.00002 4.08333Z" stroke="black" stroke-linecap="round" stroke-linejoin="round"/>
        <path d="M7 4.08333H9.625C10.0118 4.08333 10.3827 3.92969 10.6562 3.6562C10.9297 3.38271 11.0833 3.01177 11.0833 2.625C11.0833 2.23823 10.9297 1.86729 10.6562 1.5938C10.3827 1.32031 10.0118 1.16667 9.625 1.16667C7.58333 1.16667 7 4.08333 7 4.08333Z" stroke="black" stroke-linecap="round" stroke-linejoin="round"/>
        </svg>
        ${user.age}</li>
        </ul>
        </div>
        `;
    }).join('');
    usersMarkup += `<div class="addUser" id="addUser">
    <svg width="40" height="40" viewBox="0 0 40 40" fill="none" xmlns="http://www.w3.org/2000/svg">
        <g opacity="0.16">
        <path d="M26.6667 35V31.6667C26.6667 29.8986 25.9643 28.2029 24.714 26.9526C23.4638 25.7024 21.7681 25 20 25H8.33332C6.56521 25 4.86952 25.7024 3.61928 26.9526C2.36904 28.2029 1.66666 29.8986 1.66666 31.6667V35" stroke="black" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/>
        <path d="M14.1667 18.3333C17.8486 18.3333 20.8333 15.3486 20.8333 11.6667C20.8333 7.98477 17.8486 5 14.1667 5C10.4848 5 7.5 7.98477 7.5 11.6667C7.5 15.3486 10.4848 18.3333 14.1667 18.3333Z" stroke="black" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/>
        <path d="M33.3333 13.3333V23.3333" stroke="black" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/>
        <path d="M38.3333 18.3333H28.3333" stroke="black" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/>
        </g>
        </svg>
</div>`
    document.getElementById("listUsers").innerHTML = usersMarkup;
}