import { User } from "./user";
import { renderUsers } from "./user";
import axios from "axios";
var users = new Array();

document.body.addEventListener("click", (event) => {
    if (event.target.closest('.addUser') || event.target.closest('#btn')) {
        axios.get("https://randomuser.me/api/").then((response) => {
            let user = new User(response.data.results[0].name.first, response.data.results[0].name.last, response.data.results[0].email, response.data.results[0].gender, response.data.results[0].dob.age, response.data.results[0].picture.large);
            users.push(user);
            renderUsers(users);
        }).catch((error) => {
            console.log(error);
        })
    }
})